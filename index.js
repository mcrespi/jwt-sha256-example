'use strict';
const jwt = require('jsonwebtoken');
const fs = require('fs')


// ---------------------------------------------------------------------------------
// Generación del JWT (Server)
// ---------------------------------------------------------------------------------

// Payload:
const payload = {
  tipoDocumento: 'DNI',
  numeroDocumento: 24170633,
  sexo: 'M',
  apellido: 'Perez',
  nombre: 'Carlos',
  nacionalidad: 'Argentino',
  fechaNacimiento: '1970-06-20T00:00:00',
  localidad: 'CABA',
  codigoPostal: '1004',
  calle: 'Av. Corrientes',
  altura: '1615'
};

// Cargo PrivateKey
const privateKey = fs.readFileSync('./ssl/private_unencrypted.pem');

// Genero JWT RSA SHA256
const dataJWT = jwt.sign(payload, privateKey, {
  issuer: 'Seguridad Vial',
  algorithm: 'RS256', // Protocolo SHA256
  expiresIn: '24h' // Tiempo de Expiracion
});

// Muestro en Pantalla el JWT
console.log('\nJWT GENERADO:');
console.log(dataJWT);



// ---------------------------------------------------------------------------------
// A continuación se ejecuta el Proceso de Validación del JWT
// mediante la ClavePublica
// ---------------------------------------------------------------------------------

// Cargo PrivateKey
const publicKey = fs.readFileSync('./ssl/public.pem');


jwt.verify(dataJWT, publicKey, { algorithms: ['RS256'] }, function (err, payload) {
  if (err) throw new Error('JWT Invalido::' + err.message)

  // Valid JWT
  console.log('\nJWT::VALIDO')
  console.log('JWT::Payload:')
  console.log(payload);
});





