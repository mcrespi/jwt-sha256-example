# Ejemplo de Generación y Validación JWT-SHA256


## Estructura y Documentación:
* El directorio **ssl** contiene los archivos SSL (pem).
* En el archivo **index.js** se encuentra el código de la aplicación.



## Instalación y Ejecución:

### Requerimientos:
- NodeJS 4+


### Clonar el Repositorio:
```
  $ git clone https://... jwt-sha256-example
  $ cd jwt-sha256-example
```

### Instalar librerias:
```
  $ npm install
```


### Ejecución:
```
  $ npm start
```

### Ejemplo de Salida en Patalla:

```
$ npm start
> node index.js

JWT GENERADO:
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0aXBvRG9jdW1lbnRvIjoiRE5JIiwibnVtZXJvRG9jdW1lbnRvIjoyNDE3MDYzMywic2V4byI6Ik0iLCJhcGVsbGlkbyI6IlBlcmV6Iiwibm9tYnJlIjoiQ2FybG9zIiwibmFjaW9uYWxpZGFkIjoiQXJnZW50aW5vIiwiZmVjaGFOYWNpbWllbnRvIjoiMTk3MC0wNi0yMFQwMDowMDowMCIsImxvY2FsaWRhZCI6IkNBQkEiLCJjb2RpZ29Qb3N0YWwiOiIxMDA0IiwiY2FsbGUiOiJBdi4gQ29ycmllbnRlcyIsImFsdHVyYSI6IjE2MTUiLCJpYXQiOjE1NDIzOTk3MTEsImV4cCI6MTU0MjQ4NjExMSwiaXNzIjoiU2VndXJpZGFkIFZpYWwifQ.AB6y9t5mPOu2FXlLwqqEewPRkGmwsX4ti08BQONSi3Wu0P7Eau8s5_Jqj8K6clhtUNy00vhL7POE5VsuQK-lV9MMyPxE1r8nUo6-LZvtEQ0SeZKSseHKjnpBPvEadNvPMekLhA_QIzPrwdIwQIHWEZ5e0KKWpyz0znprsYoF9Q-1QJIJ6Bnh8dYOzT_-twxQtEXde-2Lhz78SOaKQl-7lscmo2zYRSWkjyxNVRfDdNXUy0LqatnHSBagYCVyUfQBFPygE68vVCYW4JRsaLTm5-Z9bOhnR3AhAlHxoSFIFpDDFeqBo8-YEG9MTylw_gZNwWiuIpCaE7VVItf9E_-RBA

JWT::Valido
JWT::Payload:
{ tipoDocumento: 'DNI',
  numeroDocumento: 24170633,
  sexo: 'M',
  apellido: 'Perez',
  nombre: 'Carlos',
  nacionalidad: 'Argentino',
  fechaNacimiento: '1970-06-20T00:00:00',
  localidad: 'CABA',
  codigoPostal: '1004',
  calle: 'Av. Corrientes',
  altura: '1615',
  iat: 1542399711,
  exp: 1542486111,
  iss: 'Seguridad Vial' }

```



## Misc:
Comandos de Ejemplo OPENSSL Genaración de RSA:

```
	$ openssl genrsa -des3 -out private.pem 2048
	$ openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

El siguiente comando desencripta la clave privada para ser leida por el codigo.
Para este ejemplo es necesario.

```
	$ openssl rsa -in private.pem -out private_unencrypted.pem -outform PEM
```

